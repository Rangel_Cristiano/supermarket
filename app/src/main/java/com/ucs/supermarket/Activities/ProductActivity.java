package com.ucs.supermarket.Activities;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.ucs.supermarket.Fragments.ProductDetailDialogFragment;
import com.ucs.supermarket.Fragments.ProductFragment;
import com.ucs.supermarket.Models.Product;
import com.ucs.supermarket.R;

public class ProductActivity extends AppCompatActivity
        implements ProductFragment.OnListFragmentInteractionListener {

    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private ProgressBar spinner;
    private boolean inProcess = false;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Cadastro de Produtos");
            getSupportActionBar() .setDisplayHomeAsUpEnabled(true);
        }

        mContext = this;

        spinner = (ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        if (recyclerViewAdapter == null) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
            recyclerView = (RecyclerView) currentFragment.getView();
            recyclerViewAdapter = ((RecyclerView) currentFragment.getView()).getAdapter();

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);

            recyclerView.addItemDecoration(dividerItemDecoration);
        }

        floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = ProductDetailDialogFragment.newInstance();
                newFragment.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (spinner != null && !inProcess)
            spinner.setVisibility(View.GONE);
    }


    @Override
    public void onListFragmentInteraction(Product product) {
        DialogFragment newFragment = ProductDetailDialogFragment.newInstance(product);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }
}

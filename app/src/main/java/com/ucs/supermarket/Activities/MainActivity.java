package com.ucs.supermarket.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import com.ucs.supermarket.Fragments.CartFragment;
import com.ucs.supermarket.Fragments.ProductDetailDialogFragment;
import com.ucs.supermarket.Models.CartAndProduct;
import com.ucs.supermarket.R;

public class MainActivity extends AppCompatActivity implements CartFragment.OnListFragmentInteractionListener {

    private Activity mActivity;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawer;
    private NavigationView mNavigationView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private ProgressBar spinner;
    private boolean inProcess = false;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Lista de Compras");

        mActivity = this;

        mDrawerLayout = findViewById(R.id.activity_main);
        mActionBarDrawer = new ActionBarDrawerToggle(this, mDrawerLayout,R.string.open, R.string.close);

        mDrawerLayout.addDrawerListener(mActionBarDrawer);
        mActionBarDrawer.syncState();

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        if (recyclerViewAdapter == null) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
            recyclerView = (RecyclerView) currentFragment.getView();
            recyclerViewAdapter = ((RecyclerView) currentFragment.getView()).getAdapter();

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);

            recyclerView.addItemDecoration(dividerItemDecoration);
        }

        floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, SelectProductActivity.class);
                startActivity(intent);
            }
        });


        mNavigationView = findViewById(R.id.nv);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                mDrawerLayout.closeDrawers();

                if (id == R.id.menu_products) {
                    Intent intent = new Intent(mActivity, ProductActivity.class);
                    startActivity(intent);
                }
                else if (id == R.id.menu_markets) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=mercado"));
                    startActivity(intent);
                }
                else if (id == R.id.menu_exit) {
                    mActivity.finish();
                } else {
                    return true;
                }

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mActionBarDrawer.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (spinner != null && !inProcess)
            spinner.setVisibility(View.GONE);

    }

    @Override
    public void onListFragmentInteraction(CartAndProduct item) {
        DialogFragment newFragment = ProductDetailDialogFragment.newInstance(item.product);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }
}

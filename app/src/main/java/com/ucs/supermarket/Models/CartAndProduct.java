package com.ucs.supermarket.Models;

import androidx.room.Embedded;
import androidx.room.Relation;

public class CartAndProduct {
    @Embedded public Product product;
    @Relation(
            parentColumn = "productId",
            entityColumn = "cartId"
    )
    public Cart cart;
}

package com.ucs.supermarket.Models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(foreignKeys = @ForeignKey(entity = Product.class,
        parentColumns = "productId",
        childColumns = "cartId",
        onDelete = ForeignKey.CASCADE))
public class Cart implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "cartId")
    private int cartId;

    @ColumnInfo(name = "productId")
    private int productId;

    @ColumnInfo(name = "quantity")
    private int quantity;

    @ColumnInfo(name = "isChecked")
    private boolean isChecked;



    public Cart() {
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getCartId() {
        return cartId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}

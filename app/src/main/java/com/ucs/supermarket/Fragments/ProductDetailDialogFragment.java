package com.ucs.supermarket.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.ucs.supermarket.Database.AppDatabase;
import com.ucs.supermarket.Models.Product;
import com.ucs.supermarket.R;

public class ProductDetailDialogFragment extends DialogFragment {

    private ImageView mImageView;
    private EditText mTitleEditView, mPriceEditText, mUnityEditText, mObservationEditText;
    private Product mProduct;
    private ProgressBar mProgressBar;

    private static String KEY_PRODUCT = "product";
    private static String DEFAULT_IMAGE = "https://app-mercado.s3.us-east-2.amazonaws.com/random.png";

    public static ProductDetailDialogFragment newInstance(Product product) {
        final ProductDetailDialogFragment productDetailDialogFragment = new ProductDetailDialogFragment();

        Bundle args = new Bundle();
        args.putSerializable(KEY_PRODUCT, product);
        productDetailDialogFragment.setArguments(args);

        return productDetailDialogFragment;
    }

    public static ProductDetailDialogFragment newInstance() {
        final ProductDetailDialogFragment productDetailDialogFragment = new ProductDetailDialogFragment();

        return productDetailDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_product_detail, null);

        mImageView = view.findViewById(R.id.photo_view);
        mTitleEditView = view.findViewById(R.id.title_edit_text);
        mPriceEditText = view.findViewById(R.id.price_edit_text);
        mUnityEditText = view.findViewById(R.id.unity_edit_text);
        mObservationEditText = view.findViewById(R.id.observation_edit_text);
        mProgressBar = view.findViewById(R.id.progressBarDetail);

        mProgressBar.setVisibility(View.GONE);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        if (getArguments() != null)
        {
            mProduct = (Product) getArguments().getSerializable(KEY_PRODUCT);

            alertDialogBuilder.setTitle("Editar Produto");

            Picasso.with(getActivity()).load(mProduct.getImage()).into(mImageView);

            mTitleEditView.setText(mProduct.getName());
            mPriceEditText.setText(mProduct.getPrice());
            if (mProduct.getUnity() != null && !mProduct.getUnity().isEmpty())
                mUnityEditText.setText(mProduct.getUnity());
            mObservationEditText.setText(mProduct.getObservation());

        }
        else {
            alertDialogBuilder.setTitle("Cadastrar Produto");

            Picasso.with(getActivity()).load(DEFAULT_IMAGE).into(mImageView);
        }

        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Salvar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if (mProduct == null) {
                    Product product = new Product();
                    product.setName(mTitleEditView.getText().toString());
                    product.setImage(DEFAULT_IMAGE);
                    product.setUnity(mUnityEditText.getText().toString());
                    product.setPrice(mPriceEditText.getText().toString());
                    product.setObservation(mObservationEditText.getText().toString());

                    AppDatabase.getAppDatabase(getContext()).productDao().insert(product);

                    Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.main_fragment);
                    if (currentFragment instanceof ProductFragment)
                        ((ProductFragment) currentFragment).addProduct(product);

                    Toast.makeText(getContext(), "Produto cadastrado!", Toast.LENGTH_LONG).show();

                } else {
                    mProduct.setProductId(mProduct.getProductId());
                    mProduct.setName(mTitleEditView.getText().toString());
                    mProduct.setUnity(mUnityEditText.getText().toString());
                    mProduct.setPrice(mPriceEditText.getText().toString());
                    mProduct.setObservation(mObservationEditText.getText().toString());

                    AppDatabase.getAppDatabase(getContext()).productDao().update(mProduct);

                    Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.main_fragment);
                    if (currentFragment instanceof ProductFragment)
                        ((ProductFragment) currentFragment).notifyDataChanged();

                    Toast.makeText(getContext(), "As alterações foram salvas!", Toast.LENGTH_LONG).show();

                }

                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        return alertDialogBuilder.create();
    }
}

package com.ucs.supermarket.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ucs.supermarket.Adapters.CartAdapter;
import com.ucs.supermarket.Database.AppDatabase;
import com.ucs.supermarket.Models.CartAndProduct;
import com.ucs.supermarket.R;

import java.util.List;

public class CartFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private CartAdapter mCartAdapter;
    private RecyclerView mRecyclerView;

    public CartFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mylist_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

            List<CartAndProduct> items = AppDatabase.getAppDatabase(getContext()).cartDao().getCartAndProduct();
            mCartAdapter = new CartAdapter(
                    items,
                    mListener,
                    getActivity());

            mRecyclerView.setAdapter(mCartAdapter);
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_cart, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mCartAdapter != null) {
            List<CartAndProduct> items = AppDatabase.getAppDatabase(getContext()).cartDao().getCartAndProduct();

            mCartAdapter.notifyDataChanged(items);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clean_all:
                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Alerta");
                alertDialog.setMessage("Tem certeza que deseja excluir todos os items comprados?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Sim",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                AppDatabase.getAppDatabase(getContext()).cartDao().deleteCheckedItems();

                                if (mCartAdapter != null) {
                                    List<CartAndProduct> items = AppDatabase.getAppDatabase(getContext()).cartDao().getCartAndProduct();

                                    mCartAdapter.notifyDataChanged(items);
                                }

                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();


                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }


    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(CartAndProduct item);
    }
}

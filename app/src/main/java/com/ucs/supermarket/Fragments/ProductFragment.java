package com.ucs.supermarket.Fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ucs.supermarket.Adapters.ProductsAdapter;
import com.ucs.supermarket.Database.AppDatabase;
import com.ucs.supermarket.Models.Product;
import com.ucs.supermarket.R;

import java.util.List;

public class ProductFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private ProductsAdapter mProductsAdapter;
    private RecyclerView mRecyclerView;

    public ProductFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

            List<Product> products = AppDatabase.getAppDatabase(getContext()).productDao().getAll();

            mProductsAdapter = new ProductsAdapter(
                    products,
                    mListener,
                    getActivity());

            mRecyclerView.setAdapter(mProductsAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void addProduct(Product product) {
        mProductsAdapter.addProduct(product);
    }

    public void notifyDataChanged() {
        List<Product> products = AppDatabase.getAppDatabase(getContext()).productDao().getAll();

        mProductsAdapter.notifyDataChanged(products);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Product item);
    }
}

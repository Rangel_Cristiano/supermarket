package com.ucs.supermarket.Fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ucs.supermarket.Adapters.SelectProductsAdapter;
import com.ucs.supermarket.Database.AppDatabase;
import com.ucs.supermarket.Models.CartAndProduct;
import com.ucs.supermarket.Models.Product;
import com.ucs.supermarket.R;

import java.util.List;

public class SelectProductsFragment extends Fragment {

    private SelectProductsFragment.OnListFragmentInteractionListener mListener;
    private SelectProductsAdapter mProductsAdapter;
    private RecyclerView mRecyclerView;

    public SelectProductsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

            List<Product> products = AppDatabase.getAppDatabase(getContext()).productDao().getAll();

            mProductsAdapter = new SelectProductsAdapter(
                    products,
                    mListener,
                    getActivity());

            mRecyclerView.setAdapter(mProductsAdapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SelectProductsFragment.OnListFragmentInteractionListener) {
            mListener = (SelectProductsFragment.OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Product item);
    }
}

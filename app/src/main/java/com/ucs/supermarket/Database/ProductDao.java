package com.ucs.supermarket.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.ucs.supermarket.Models.Cart;
import com.ucs.supermarket.Models.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM product ORDER BY name")
    List<Product> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Product... products);

    @Update
    void update(Product product);

    @Delete
    void delete(Product product);
}

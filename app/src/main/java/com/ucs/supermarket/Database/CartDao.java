package com.ucs.supermarket.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.ucs.supermarket.Models.Cart;
import com.ucs.supermarket.Models.CartAndProduct;

import java.util.List;

@Dao
public interface CartDao {

    @Query("SELECT * FROM Cart ")
    List<Cart> getAll();

    @Query("UPDATE Cart SET isChecked = :isChecked WHERE cartId = :id ")
    int setCheckedItem(int id, boolean isChecked);

    @Query("SELECT * FROM Cart LEFT JOIN Product ON Cart.cartId = Product.productId ORDER BY isChecked, Product.name")
    List<CartAndProduct> getCartAndProduct();

    @Query("SELECT * FROM Cart LEFT JOIN Product ON Cart.cartId = Product.productId WHERE Product.productId = :productId")
    CartAndProduct getCartAndProduct(int productId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Cart... carts);

    @Delete
    void delete(Cart cart);

    @Query("DELETE FROM Cart WHERE isChecked")
    void deleteCheckedItems();
}
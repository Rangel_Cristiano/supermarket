package com.ucs.supermarket.Database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.ucs.supermarket.Models.Cart;
import com.ucs.supermarket.Models.Product;

import java.util.concurrent.Executors;

@Database(entities = {Product.class, Cart.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;
    private static Context mContext;
    private static final Object LOCK = new Object();

    public abstract ProductDao productDao();

    public abstract CartDao cartDao();

    public static AppDatabase getAppDatabase(final Context context) {
        mContext = context;

        synchronized (LOCK) {
            if (INSTANCE == null) {
                INSTANCE =
                        Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "db-supermarket")
                                .allowMainThreadQueries()
                                .fallbackToDestructiveMigration()
                                .addCallback(new Callback() {
                                    @Override
                                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                        super.onCreate(db);

                                        Executors.newSingleThreadExecutor().execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                addProducts(context);
                                            }
                                        });
                                    }
                                }).build();
            }
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    private static RoomDatabase.Callback dbCallback = new RoomDatabase.Callback() {
        public void onCreate(SupportSQLiteDatabase db) {

            Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    addProducts(mContext);
                }
            });
        }
    };

    private static void addProducts(Context context) {
        Product product = new Product();

        product.setName("Macarrão");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/macarrao.jpeg");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Carne");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/carne.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Frango");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/frango.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Pizza");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/pizza.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Abacaxi");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/abacaxi.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Batata");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/batata.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Cerveja");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/cerveja.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Biscoito");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/biscoito.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Café");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/beber.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Chocolate");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/chocolate.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Suco");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/comida-e-restaurante.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Açucar");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/doce.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Água");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/garrafa-de-agua.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Salgadinho");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/lanche.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Leite");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/leite.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Maça");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/maca.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Óleo de cozinha");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/oleo-de-dippel.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Ovos");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/ovos.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Pão");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/pao.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Papel higiênico");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/papel-higienico.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Papel toalha");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/papel-toalha.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Refrigerante");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/refri.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Pasta de dente");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/pasta-de-dentes.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Molho de tomate");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/pode.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Queijo");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/queijo.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Sabonete");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/sabonete.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Sal");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/sal.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Sorvete");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/sobremesa.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Shampoo");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/xampu.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Produto de limpeza");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/limpeza.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Detergente");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/detergente.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Sabão em pó");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/sabao-em-po.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Banana");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/bananas.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Tomate");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/tomate.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Manteiga");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/manteiga.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Doce de leite");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/doce-de-leite.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Alface");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/comida.png");

        getAppDatabase(context).productDao().insert(product);

        product.setName("Pipoca");
        product.setImage("https://app-mercado.s3.us-east-2.amazonaws.com/cinema.png");

        getAppDatabase(context).productDao().insert(product);

    }
}
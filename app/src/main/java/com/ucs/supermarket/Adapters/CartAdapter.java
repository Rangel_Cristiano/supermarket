package com.ucs.supermarket.Adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ucs.supermarket.Database.AppDatabase;
import com.ucs.supermarket.Fragments.CartFragment;
import com.ucs.supermarket.Models.CartAndProduct;
import com.ucs.supermarket.R;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private List<CartAndProduct> mItems;
    private final CartFragment.OnListFragmentInteractionListener mListener;
    private Activity mActivity;

    public CartAdapter(
            List<CartAndProduct> items,
            CartFragment.OnListFragmentInteractionListener listener,
            Activity activity) {

        mItems = items;
        mListener = listener;
        mActivity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_mylist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mItems.get(position);

        if (holder.mItem.product != null)
        {
            int quantity = holder.mItem.cart.getQuantity();

            String unity = "UN";
            if (holder.mItem.product.getUnity() != null && !holder.mItem.product.getUnity().isEmpty())
                unity = holder.mItem.product.getUnity();

            holder.mQuantityTextView.setText(String.valueOf(quantity) + " " + unity);

            holder.mTitleTextView.setText(holder.mItem.product.getName());

            Picasso.with(mActivity)
                    .load(holder.mItem.product.getImage())
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.mImageView);

            holder.mCheckboxView.setChecked(holder.mItem.cart.isChecked());

            holder.mCheckboxView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppDatabase.getAppDatabase(mActivity).cartDao().setCheckedItem(
                            holder.mItem.cart.getProductId(),
                            holder.mCheckboxView.isChecked());

                    notifyDataChanged(AppDatabase.getAppDatabase(mActivity).cartDao().getCartAndProduct());
                }
            });

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        mListener.onListFragmentInteraction(holder.mItem);
                    }
                }
            });
        }
    }

    public void notifyDataChanged(List<CartAndProduct> products) {
        mItems = products;

        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleTextView;
        public final CheckBox mCheckboxView;
        public final TextView mQuantityTextView;
        public CartAndProduct mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.photo_image_view);
            mTitleTextView = view.findViewById(R.id.title_text_view);
            mCheckboxView = view.findViewById(R.id.is_checked_checkbox);
            mQuantityTextView = view.findViewById(R.id.quantity_text_view);
        }
    }
}

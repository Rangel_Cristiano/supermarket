package com.ucs.supermarket.Adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ucs.supermarket.Fragments.ProductFragment.OnListFragmentInteractionListener;
import com.ucs.supermarket.Models.Product;
import com.ucs.supermarket.R;

import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<Product> mProducts;
    private final OnListFragmentInteractionListener mListener;
    private Activity mActivity;

    public ProductsAdapter(
            List<Product> products,
            OnListFragmentInteractionListener listener,
            Activity activity) {

        mProducts = products;
        mListener = listener;
        mActivity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mProduct = mProducts.get(position);

        String title = mProducts.get(position).getName();

        holder.mTitleTextView.setText(title);

        Picasso.with(mActivity)
                .load(mProducts.get(position).getImage())
                .placeholder(R.drawable.progress_animation)
                .into(holder.mImageView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mProduct);
                }
            }
        });
    }

    public void addProduct(Product product) {
        if (mProducts != null)
            mProducts.add(0, product);
        else {
            mProducts = new ArrayList<>();

            mProducts.add(0, product);
        }

        this.notifyItemInserted(0);
    }

    public void notifyDataChanged(List<Product> products) {
        mProducts = products;

        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mProducts == null ? 0 : mProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleTextView;
        public Product mProduct;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.photo_image_view);
            mTitleTextView = view.findViewById(R.id.title_text_view);
        }
    }
}

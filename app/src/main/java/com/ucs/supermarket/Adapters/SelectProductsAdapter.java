package com.ucs.supermarket.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ucs.supermarket.Database.AppDatabase;
import com.ucs.supermarket.Fragments.SelectProductsFragment.OnListFragmentInteractionListener;
import com.ucs.supermarket.Models.Cart;
import com.ucs.supermarket.Models.CartAndProduct;
import com.ucs.supermarket.Models.Product;
import com.ucs.supermarket.R;

import java.util.List;

public class SelectProductsAdapter extends RecyclerView.Adapter<SelectProductsAdapter.ViewHolder> {

    private List<Product> mProducts;
    private final OnListFragmentInteractionListener mListener;
    private Activity mActivity;

    public SelectProductsAdapter(
            List<Product> products,
            OnListFragmentInteractionListener listener,
            Activity activity) {

        mProducts = products;
        mListener = listener;
        mActivity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_select_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mProduct = mProducts.get(position);

        CartAndProduct cartAndProduct = AppDatabase.getAppDatabase(mActivity).cartDao().getCartAndProduct(holder.mProduct.getProductId());
        holder.mQuantityEditText.setText(cartAndProduct == null || cartAndProduct.cart == null ? "0" : String.valueOf(cartAndProduct.cart.getQuantity()));

        holder.mTitleTextView.setText(mProducts.get(position).getName());
        Picasso.with(mActivity)
                .load(mProducts.get(position).getImage())
                .placeholder(R.drawable.progress_animation)
                .into(holder.mImageView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mProduct);
                }
            }
        });

        holder.mPlusImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = Integer.valueOf(holder.mQuantityEditText.getText().toString());
                quantity = quantity + 1;

                holder.mQuantityEditText.setText(String.valueOf(quantity));

                saveProductOnCart(holder.mProduct.getProductId(), quantity);
            }
        });

        holder.mMinusImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = Integer.valueOf(holder.mQuantityEditText.getText().toString());
                quantity = quantity - 1;

                if (quantity >= 0)
                {
                    holder.mQuantityEditText.setText(String.valueOf(quantity));

                    saveProductOnCart(holder.mProduct.getProductId(), quantity);
                }
            }
        });
    }

    public void saveProductOnCart(int id, int quantity)
    {
        Cart cart = new Cart();
        cart.setCartId(id);
        cart.setProductId(id);
        cart.setQuantity(quantity);
        cart.setChecked(false);

        if (quantity == 0) {
            AppDatabase.getAppDatabase(mActivity).cartDao().delete(cart);
        } else {
            AppDatabase.getAppDatabase(mActivity).cartDao().insert(cart);
        }
    }

    public void notifyDataChanged(List<Product> products) {
        mProducts = products;

        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mProducts == null ? 0 : mProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleTextView;
        public final ImageView mPlusImageView;
        public final ImageView mMinusImageView;
        public final EditText mQuantityEditText;
        public Product mProduct;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.photo_image_view);
            mTitleTextView = view.findViewById(R.id.title_text_view);
            mPlusImageView = view.findViewById(R.id.plus_imageView);
            mMinusImageView = view.findViewById(R.id.minus_imageView);
            mQuantityEditText =  view.findViewById(R.id.quantity_edit_text);
        }
    }
}
